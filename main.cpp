/*
* Multiple line
* comment
*/
#include<iostream>
#include <cmath>
#include <fstream>

//Single line comment
using namespace std;
#include "linsyspb.h"
#include "preconditioning.h"

// ****************** Some functions ******************
// // dot product
// double dotprod(double *x, double *y, int size) {
//   double alpha = 0.;
//   for (int irow = 0; irow < size; irow++) {
//     alpha += x[irow]*y[irow];
//   }
//   return alpha;
// }

// ****************** Main Program ******************
//This is where the execution of program begins
int main(int argc, char* argv[])
{

   // Create an object LinSysPb from matrix market file :
   // int prectype = stoi(argv[1]);
   int prectype = 0;
   LinSysPb Axb("/Users/nguilber/Documents/WarmUpLinAlg/MatrixCollection/gr_30_30.mtx", prectype);
   Axb.printdim();
   int ncols = Axb.getncols();
   int nlines = Axb.getnlines();
   Preconditioning Prec(prectype, Axb.getIAptr(), Axb.getJAptr(), Axb.getAAptr(), nlines);
   Prec.printprectype();
   if (ncols != nlines ) {
     cout << "!!!!!!!!!!!! Warning : Rectangular problem" << endl;
   }
   Axb.genrandomrhs(0,1);

   double xsol[ncols];
   double eps  = 1.e-6;
   int maxiter = 50;
   // Initialization of solution
   for (int i = 0; i < ncols; i++) {
     xsol[i] = 0.;
   }

   // Check Matrix on screen
   // Axb.printfullmat();

   if (prectype == 0) {
     // Solve with CG algorithm
     Axb.solvebyCG(&xsol[0], eps, maxiter);
     Axb.checksolandres(&xsol[0]);
   }
   else{
     // Solve with PCG
     Prec.prepareJacobiPrec();
     Axb.solvebyPCG(&xsol[0], eps, maxiter, prectype, Prec);
     Axb.checksolandres(&xsol[0]);
   }



   // ********************************************
   // Check mat vec prod and dot product
   // double *xtest = new double[ncols];
   // double *ytest = new double[nlines];
   //
   // for (size_t i = 0; i < nlines; i++) {
   //   xtest[i] = 0.5;
   //   ytest[i] = 1.0;
   // }
   // Axb.matvecprod(xtest,ytest);
   // double alpha = dotprod(xtest,ytest,nlines);
   // cout << alpha << " ---- " << nlines*0.5 << endl;
   // ofstream myfile;
   // myfile.open("resx05.dat");
   // if (myfile.is_open()) {
   //   for (size_t i = 0; i < nlines; i++) {
   //     myfile << ytest[i] << '\n';
   //   }
   // }
   // myfile.close();
   // delete [] xtest;
   // delete [] ytest;

   // ********************************************
   // Check random rhs
   // Axb.genrandomrhs(0,10);

   return 0;
}
