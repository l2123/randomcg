#include "linsyspb.h"
// Definition, storage and solving linear problems

// ****************** Some functions ******************
// dot product
double dotprod(double *x, double *y, int size) {
  double alpha = 0.;
  for (int irow = 0; irow < size; irow++) {
    alpha += x[irow]*y[irow];
  }
  return alpha;
}

// ***********************************************************
// Build linear system from a matrix market file
LinSysPb::LinSysPb(string filename, int prectype)
{
  ifstream file(filename);

  // Ignore comments headers
  while (file.peek() == '%') file.ignore(2048, '\n');

  // Read number of rows and columns
  int a,b,c;
  file >> a >> b >> c;
  _nlines = b;
  _ncols = a;
  _nnz = c;
  cout << _nlines << " " << _ncols << endl;

  // Create arrays for non-zeros values
  _AA = new double[_nnz];
  _IA = new int[_nlines+1];
  _JA = new int[_nnz];
  _b  = new double[_nlines];
  _IA[0] = 0;
  _IA[1] = 0;

  // fill the matrix with data
  int row = 0;
  for (int count = 0; count < _nnz; count++)
  {
      double data;
      int row_new, col_new;
      file >> col_new >> row_new >> data;
      _AA[count] = data;
      _JA[count] = col_new-1;
      if (row == (row_new-1)) {
        _IA[row+1]++;
      }
      else if (row < (_nlines-1)){
        _IA[row+2] = _IA[row+1];
        _IA[row+2]++;
        row++;
      }
  }
  file.close();
  _prectype = prectype;
}

LinSysPb::~LinSysPb()
{
    delete [] _IA;
    delete [] _JA;
    delete [] _AA;
    delete [] _b;
}

// ***********************************************************
// Print size of the pb

void LinSysPb::printdim()
{
	cout << "number of lines = " << _nlines << endl;
	cout << "number of columns = " << _ncols << endl;
	cout << "number of non-zero elements = " << _nnz << endl;
}

// ***********************************************************
// Print full matrix

void LinSysPb::printfullmat(){
  for (int irow = 0; irow < _nlines; irow++) {
    cout << "Line Number " << irow << endl;
    for (int j = _IA[irow]; j < _IA[irow+1]; j++) {
      cout << _JA[j] << endl;
      cout << _AA[j] << endl;
    }
  }
}
// ***********************************************************
// Matrix vector product

void LinSysPb::matvecprod(double* x, double* y)
{
  for (int irow = 0; irow < _nlines; irow++) {
    y[irow] = 0.;
  }
  for (int irow = 0; irow < _nlines; irow++) {
    int begline = _IA[irow];
    int endline = _IA[irow+1];

    y[irow] += _AA[begline]*x[irow];
    for (int j = begline+1; j < endline; j++) {
      int jcol = _JA[j];
      y[irow] += _AA[j]*x[jcol];
      y[jcol] += _AA[j]*x[irow];
    }
  }
}

// ***********************************************************
// Generate random right hand side

void LinSysPb::genrandomrhs(int mini, int maxi)
{
  // Modify as needed
  // constexpr int MIN = mini;
  // constexpr int MAX = maxi;
  std::random_device rd;
  std::default_random_engine eng(rd());
  std::uniform_real_distribution<double> distr(mini, maxi);
  double norm = 0.;

  for (int irow = 0; irow < _nlines; irow++) {
    _b[irow] = distr(eng);
    norm += _b[irow]*_b[irow];
  }
  for (int irow = 0; irow < _nlines; irow++) {
    _b[irow] =  _b[irow]/sqrt(norm);
  }

}

// ***********************************************************
// CG Algo without preconditioner

void LinSysPb::solvebyCG(double *xsol, double eps, int itermax)
{

  double resNorm    = 0.;
  double resNormNew = 0.;
  double resNorm0;
  double res[_nlines]; //residual vec
  double p  [_ncols ]; //direction
  double Ap [_nlines]; //A.p mat-vec product

  // Initialization
  this->matvecprod(xsol, &res[0]); //A.x_0
  for (int i = 0; i < _nlines; i++) {
    res[i]   = _b[i]-res[i]; // r_0 = b - A.x_0
    p[i]     = res[i];       // p_0 = r_0
    resNorm += res[i]*res[i];
  }
  resNorm0 = sqrt(resNorm);
  int itercount = 0;
  cout << "Initial Residual : " << resNorm0 << endl;
  // Main Loop
  while ((sqrt(resNorm) > eps) && (itercount <= itermax)) {

    // ----------------------------------------------
    //      Update solution and residual
    this->matvecprod(&p[0], &Ap[0]); //A.p
    // Compute <r,r>/<p,Ap>
    double alpha_k = resNorm/dotprod(&p[0], &Ap[0],_nlines);
    //Update solution and residual
    resNormNew = 0.;
    for (int i = 0; i < _nlines; i++) {
      xsol[i]   = xsol[i] + alpha_k*p[i];
      res[i]    = res[i]  - alpha_k*Ap[i];
      resNormNew += res[i]*res[i];
    }
    cout << "Iteration number " << itercount << endl;
    cout << "Residual : " << scientific << sqrt(resNormNew) << endl;
    //-----------------------------------------------
    //               Update Direction
    double beta_k = (resNormNew/resNorm);
    for (int i = 0; i < _nlines; i++) {
      p[i] = res[i] + beta_k*p[i];
    }
    resNorm = resNormNew;
    itercount++;

  }

}

// *********************************************************
//      Check solution with b-Ax operation
void LinSysPb::checksolandres(double *xsol) {
  double res[_nlines];
  double resnorm = 0.;
  this->matvecprod(xsol, &res[0]);

  for (int i = 0; i < _nlines; i++) {
    res[i] = _b[i] - res[i];
    resnorm += res[i]*res[i];
  }
  cout << scientific << "Check Final Residual b-Ax : " << sqrt(resnorm) << endl;
}

// ***********************************************************
// PCG Algo

void LinSysPb::solvebyPCG(double *xsol, double eps, int itermax, int prectype, Preconditioning &Prec)
{

  double resNorm    = 0.;
  double resNormNew = 0.;
  double resNorm0;
  double res[_nlines]; // Residual vec
  double p  [_ncols ]; // Direction
  double Ap [_nlines]; // A.p mat-vec product
  double z  [_nlines]; // Preconditioned vector

  // Initialization
  this->matvecprod(xsol, &res[0]); //A.x_0
  for (int i = 0; i < _nlines; i++) {
    res[i]   = _b[i]-res[i]; // r_0 = b - A.x_0
    resNorm += res[i]*res[i];
  }
  // ***************************************************
  // !!!!!!!! Add preconditioning operation to build z_0
  Prec.applyJacobiPrec(&res[0],&z[0]);
  for (int i = 0; i < _nlines; i++) {
    p[i] = z[i];
  }
  // ***************************************************
  double dprz = dotprod(&z[0], &res[0],_nlines);
  double dprznew = 0;

  resNorm0 = sqrt(resNorm);
  int itercount = 0;
  cout << "Initial Residual : " << resNorm0 << endl;
  // Main Loop
  while ((sqrt(resNorm) > eps) && (itercount <= itermax)) {

    // ----------------------------------------------
    //      Update solution and residual
    this->matvecprod(&p[0], &Ap[0]); //A.p
    // Compute <r,r>/<p,Ap>
    double alpha_k = dprz/dotprod(&p[0], &Ap[0],_nlines);
    //Update solution and residual
    resNormNew = 0.;
    for (int i = 0; i < _nlines; i++) {
      xsol[i]     = xsol[i] + alpha_k*p[i] ;
      res[i]      = res[i]  - alpha_k*Ap[i];
      resNormNew += res[i]*res[i];
    }
    // ***************************************************
    // !!!!!!!! Add preconditioning operation to build z_k+1
    Prec.applyJacobiPrec(&res[0],&z[0]);
    // ***************************************************

    cout << "Iteration number " << itercount << endl;
    cout << "Residual : " << scientific << sqrt(resNormNew) << endl;
    //-----------------------------------------------
    //               Update Direction
    double dprznew = dotprod(&z[0], &res[0],_nlines);
    double beta_k  = (dprznew/dprz);
    for (int i = 0; i < _nlines; i++) {
      p[i] = z[i] + beta_k*p[i];
    }
    resNorm = resNormNew;
    dprz    = dprznew;
    itercount++;

  }

}
