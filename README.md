-> Fiche de route <-

- Premier Depot de la maquette C++ du randomized CG

- Petite classe créée, dev les fonctions et mettre en place des pointeurs pour stocker matrice et vecteurs.

- Matrice stockee et lu d'un fichier de matrix MatrixMarket
Suite : dev du produit matrice vecteur et stockage du rhs

- Tout est bien stocke a priori + On a un gradient conjugué qui est pas encore validé sur le premier cas test

- CG validé avec residu recalculé à la fin avec b-Ax. Attention la norme A de l'erreur eventuellement

- Go pour le CG precondtionné

- Nouvelle classe créée pour la construction et l'application des preconditionnements, et CG préconditionné près à être finaliser.

- Le PCG avec Jacobi semble ok mais residu bizarre à la fin à verifier
