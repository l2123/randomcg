#ifndef LINSYSPB_H
#define LINSYSPB_H

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <random>
#include <iomanip>
#include "preconditioning.h"

using namespace std;

class LinSysPb
{

  //Methods
public:
  LinSysPb(string filename, int prectype);
  ~LinSysPb();
  void printdim();
  void printprectype();
  int  getncols(){return _ncols;}
  int  getnlines(){return _nlines;}
  int  getprectype(){return _prectype;}
  int  getnnz(){return _nnz;}
  int*    getIAptr(){return _IA;}
  int*    getJAptr(){return _JA;}
  double* getAAptr(){return _AA;}
  void printfullmat();
  void matvecprod(double* x, double* y);
  void genrandomrhs(int mini, int maxi);
  void solvebyCG(double *xsol, double eps, int itermax);
  void solvebyPCG(double *xsol, double eps, int itermax, int prectype, Preconditioning &Prec);
  void checksolandres(double *xsol);
  //Attributes
private:
  int _nlines;
  int _ncols;
  int _nnz;
  int*    _IA;
  int*    _JA;
  double* _AA;
  double* _b;

  int _prectype;
};

#endif
